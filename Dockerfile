FROM node:15
USER root
WORKDIR /app

COPY . .
RUN npm install
RUN npm i eslint --global && \ 
    npm init @eslint/config   

USER node
EXPOSE 8080

CMD ["npm", "run", "start"]
